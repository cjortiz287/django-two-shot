from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from .forms import ReceiptForm

# Create your views here.
def receipt_list_view(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "recipts/receipts/receipt_list.html", context)

def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.sxave()
            return redirect("home")
    else:
        form = ReceiptForm()

    return render(request, "receipts/create_receipt.html", {"form": form})

def category_list(request):
    expense_categories = ExpenseCategory.objects.filter(user_property=request.user)
    context = {
        "expense_categories": expense_categories
    }
    return render(request, "receipts/category_list.html", context)

def account_list(request):
    accounts = Account.objects.filter(user_property=request.user)
    context = {
        "accounts": accounts
    }
    return render(request, "receipts/account_list.html", context)
