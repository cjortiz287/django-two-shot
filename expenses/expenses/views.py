from django.shortcuts import render, redirect
from .forms import ExpenseCategoryForm

def create_expense_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(commit=False)
            expense_category.owner = request.user  # Set the owner to the current user
            expense_category.save()
            return redirect('category_list')  # Redirect to the list of categories
    else:
        form = ExpenseCategoryForm()
    return render(request, 'create_category.html', {'form': form})
