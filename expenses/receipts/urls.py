from django.urls import path
from django.views.generic.base import RedirectView
from . import views

urlpatterns = [
    path("", views.receipt_list_view, name="home"),
    path("", views.receipt_list_view, name="receipt_list"),
    path("create/", views.create_receipt, name="create_receipt"),
    path("categories/", views.category_list, name="category_list"),
    path("accounts/", views.account_list, name="account_list"),
]
