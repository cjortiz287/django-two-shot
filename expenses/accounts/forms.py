from django import forms
from .models import Account

class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(widget=forms.PasswordInput)

class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
    password_confirmation = forms.CharField(max_length=150, widget=forms.PasswordInput)

class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ["name", "number"]
