from django.db import models
from .models import Receipt

# Create your models here.
class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=50)

