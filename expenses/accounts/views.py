from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.urls import reverse
from .forms import LoginForm, SignUpForm, AccountForm

# Create your views here.
def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(user=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")

    else:
        form = LoginForm()
    return render(request, "accounts/login.html", {"form": form})

def logout_view(request):
    logout(request)
    return redirect(reverse("login"))

def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password.confirmation:
                user = User.objects.create_user(username=username, password=password)
                login(request, user)
                return redirect("receipt_list")
            else:
                error = "The passwords do not match"
                return render(request, "registration/signup.html", {"form": form, "error": error})
        else:
            form = SignUpForm()
            return render(request, "registration/signup.html", {"form": form})

def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")

    else:
        form = AccountForm()
    return render(request, "create_account.html", {"form": form})
